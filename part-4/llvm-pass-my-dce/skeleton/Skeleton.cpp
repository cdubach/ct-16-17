#define DEBUG_TYPE "opCounter"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;
namespace {
   struct MyDCE : public FunctionPass {
     static char ID;
     MyDCE() : FunctionPass(ID) {}
     
     
     virtual bool runOnFunction(Function &F) {
       errs() << "BEFORE DCE\n";
       //Count before dead code elimination here
       //Implement your live variable analysis here
       errs() << "DCE START\n";
       //Eliminate dead code and produce counts here
       errs() << "DCE END\n";
       return false;
     }
   };
}
char MyDCE::ID = 0;
static RegisterPass<MyDCE> X("mydce", "My dead code elimination");

